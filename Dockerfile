FROM composer:2.8.5 as composer

FROM php:8.4.4 as base

# --------------------------------------------- Mets à jour les dépendances --------------------------------------------

RUN apt-get update && \
    apt-get upgrade -y --no-install-recommends \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# ------------------------------------------ Installe les packages nécessaires -----------------------------------------

# Pour toujours installer les dernières versions
RUN apt-get update && \
    apt-get install -y --no-install-recommends wget libicu-dev libzip-dev zip unzip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# --------------------------------------------- Installe les extensions PHP --------------------------------------------

RUN docker-php-ext-configure zip \
    && docker-php-ext-install opcache intl zip \
    && apt-get update \
    && apt-get install -y --no-install-recommends libfreetype6-dev libjpeg62-turbo-dev libpng-dev \
    && docker-php-ext-configure gd \
      --with-freetype=/usr/include/ \
      --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j "$(nproc)" gd \
    && docker-php-ext-enable gd \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# ---------------------------------------------------- Permissions -----------------------------------------------------

ARG USER_ID=1000
ARG GROUP_ID=1000

# - Configure l'utilisateur 1000 pour la compatibilité avec docker
RUN userdel -f www-data \
    && if getent group www-data ; then groupdel www-data; fi \
    && groupadd -g ${GROUP_ID} www-data \
    && useradd -l -u ${USER_ID} -g www-data www-data \
    && usermod -aG sudo www-data \
    && install -d -m 0755 -o www-data -g www-data /home/www-data \
    && chown --changes --silent --no-dereference --recursive \
          --from=33:33 ${USER_ID}:${GROUP_ID} \
        /home/www-data \
        /var/www


# --------------------------------------------------- Configuration ----------------------------------------------------

# Timezone
ENV TZ=Europe/Paris
RUN echo "date.timezone = Europe/Paris" > /usr/local/etc/php/conf.d/timezone.ini

# ------------------------------------------------------ Composer ------------------------------------------------------

RUN mkdir -p /var/www/.composer && chown -R www-data:www-data /var/www/.composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# -------------------------------------------------------- APP ---------------------------------------------------------

RUN mkdir /app && chown -R www-data:www-data /app
WORKDIR /app

RUN composer global require \
    phpstan/phpstan \
    phpmd/phpmd \
    micheh/phpcs-gitlab squizlabs/php_codesniffer \
    friendsofphp/php-cs-fixer \
    thecodingmachine/phpstan-strict-rules \
    spaze/phpstan-disallowed-calls \
    qossmic/deptrac-shim \
    pdepend/pdepend:2.15.1
