# 🐳 Image Docker PHPQA

[![pipeline status](https://gitlab.com/typo-indus/docker/phpqa/badges/main/pipeline.svg)](https://gitlab.com/typo-indus/docker/phpqa/-/commits/main)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/49210272)](https://gitlab.com/typo-indus/docker/phpqa/-/commits/main)
[![Latest Release](https://gitlab.com/typo-indus/docker/phpqa/-/badges/release.svg)](https://gitlab.com/typo-indus/docker/phpqa/-/releases)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/typo-indus/docker/phpqa/-/blob/main/LICENSE)

Cette image Docker vous permet de lancer des analyses php.

## 🚀 Utilisation

1. Récupérez l'image Docker à partir du registre Docker :

```bash
docker pull registry.gitlab.com/typo-indus/docker/phpqa:main
```

### ⚙️ Fichier de configuration de chaque outils

Si les fichiers de configuration n'existent pas ils seront ajoutés par l'outils.

#### phpstan

phpstan.neon

```yaml
parameters:
    level: 9 #valeur de 0 à 9. Sur un projet existant commencer à 0 et augmenter petit à petit le niveau.
    paths:
        - src
        - tests
```

#### phpmd

phpmd.xml

```xml
<ruleset name="phpmd"
         xmlns="http://pmd.sf.net/ruleset/1.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://pmd.sf.net/ruleset/1.0.0 http://pmd.sf.net/ruleset_xml_schema.xsd"
         xsi:noNamespaceSchemaLocation="http://pmd.sf.net/ruleset_xml_schema.xsd">
    <description>PHPMD Rules</description>

    <rule ref="rulesets/unusedcode.xml" />

    <rule ref="rulesets/naming.xml" />

    <rule ref="rulesets/codesize.xml">
        <exclude name="TooManyPublicMethods" />
    </rule>
    <rule ref="rulesets/codesize.xml/TooManyPublicMethods">
        <properties>
            <property name="ignorepattern">
                <value>(^(set|get|is|find))i</value>
            </property>
        </properties>
    </rule>
    <rule ref="rulesets/design.xml" />
    <rule ref="rulesets/controversial.xml">
        <exclude name="CamelCaseMethodName" />
    </rule>
    <rule ref="rulesets/controversial.xml/CamelCaseMethodName">
        <properties>
            <property name="allow-underscore-test">
                <value>true</value>
            </property>
        </properties>
    </rule>
</ruleset>
```

#### phpcs

phpcs.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<ruleset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:noNamespaceSchemaLocation="/root/.composer/vendor/squizlabs/php_codesniffer/phpcs.xsd">
    <arg name="basepath" value="."/>
    <arg name="cache" value=".phpcs-cache"/>
    <arg name="colors" />
    <arg name="extensions" value="php"/>

    <rule ref="PSR12">
        <exclude name="PSR2.ControlStructures.ControlStructureSpacing.SpacingAfterOpenBrace"/>
    </rule>
    <rule ref="PSR1.Methods.CamelCapsMethodName">
        <exclude-pattern>./tests/*</exclude-pattern>
    </rule>

    <file>config/</file>
    <file>public/</file>
    <file>src/</file>
    <file>tests/</file>

</ruleset>
```

#### php-cs-fixer

.php-cs-fixer.dist.php

```php
<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__ . '/src')
    ->in(__DIR__ . '/tests')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
    '@PSR12' => true,
    'full_opening_tag' => false,
])
    ->setFinder($finder)
;
```

#### deptrac

deptrac.yaml

```yaml
---
parameters:
  paths:
    - ./src

  layers:
    - name: Application
      collectors:
        - type: className
          regex: ^App\\Application\\.*
    - name: Domaine
      collectors:
        - type: className
          regex: ^App\\Domaine\\.*
    - name: Infrastructure
      collectors:
        - type: bool
          must:
            - type: className
              regex: ^App\\Infrastructure\\.*
          must_not:
            - type: className
              regex: ^App\\Infrastructure\\Provider\\.*
            - type: className
              regex: ^App\\Infrastructure\\Database\\.*
            - type: className
              regex: ^App\\Infrastructure\\FileSystem\\.*
    - name: InfrastructureDatabase
      collectors:
        - type: className
          regex: ^App\\Infrastructure\\Database\\.*
    - name: InfrastructureFileSystem
      collectors:
        - type: className
          regex: ^App\\Infrastructure\\FileSystem\\.*
    - name: InfrastructureProvider
      collectors:
        - type: bool
          must:
            - type: className
              regex: ^App\\Infrastructure\\Provider\\.*
    - name: Symfony
      collectors:
        - type: bool
          must:
            - type: className
              regex: ^Symfony\\.*
    - name: Doctrine
      collectors:
        - type: className
          regex: ^Doctrine\\.*

    - name: FileSystem
      collectors:
        - type: className
          regex: ^League\\Flysystem\\.*

    - name: ApplicationDependencies
      collectors:
        - type: className
          regex: ^JMS\\.*

    - name: InfrastuctureProviderDependencies
      collectors:
        - type: className
          regex: ^Ramsey\\.*

  ruleset:
    Domaine:
    InfrastructureProvider:
      - Domaine
      - Infrastructure
      - InfrastuctureProviderDependencies
      - InfrastructureDatabase
      - InfrastructureFileSystem
    Infrastructure:
      - Symfony
    InfrastructureDatabase:
      - Doctrine
    InfrastructureFileSystem:
      - FileSystem
    Application:
      - ApplicationDependencies
      - Domaine
      - Symfony
```

## 👥 Comment Contribuer

Pas de contribution pour le moment. Contactez-moi via les issues.

## 📄 License

Ce projet est sous licence MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails.
